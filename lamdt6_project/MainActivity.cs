﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Text;

namespace lamdt6_project
{
    [Activity(Label = "lamdt6_project", MainLauncher = true)]
    public class MainActivity : Activity
    {
        TextView txtIntro;
        Button btnKhuyenMai, btnGiamGia, btnBanChay,btnSanPham,btnLichSu;
        ImageView img_a, img_b, img_c, btnLike,img_m,img_h,img_g;
        bool isLike = false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            txtIntro = FindViewById<TextView>(Resource.Id.txtIntro);
            txtIntro.Text = Html.FromHtml(GetString(Resource.String.some_text)).ToString();
            btnKhuyenMai = FindViewById<Button>(Resource.Id.btn_khuyenMai);
            btnGiamGia = FindViewById<Button>(Resource.Id.btn_giamGia);
            btnBanChay = FindViewById<Button>(Resource.Id.btn_banChay);
            btnLike = FindViewById<ImageView>(Resource.Id.btnLike);
            btnSanPham = FindViewById<Button>(Resource.Id.btn_SanPham);
            btnLichSu = FindViewById<Button>(Resource.Id.btn_LichSu);
            img_a = FindViewById<ImageView>(Resource.Id.img_a);
            img_b = FindViewById<ImageView>(Resource.Id.img_b);
            img_c = FindViewById<ImageView>(Resource.Id.img_c);
            img_m = FindViewById<ImageView>(Resource.Id.img_m);
            img_g = FindViewById<ImageView>(Resource.Id.img_g);
            img_h = FindViewById<ImageView>(Resource.Id.img_h);

            btnBanChay.Click += BtnBanChay_Click;
            btnGiamGia.Click += BtnGiamGia_Click;
            btnKhuyenMai.Click += BtnKhuyenMai_Click;
            btnLichSu.Click += BtnLichSu_Click;
            btnSanPham.Click += BtnSanPham_Click1;
            btnLike.Click += BtnLike_Click;
        }

        private void BtnSanPham_Click1(object sender, System.EventArgs e)
        {
            Toast.MakeText(this, "Sản phẩm yêu thích !", ToastLength.Short).Show();
        }

        private void BtnLichSu_Click(object sender, System.EventArgs e)
        {
            btnSanPham.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn));
            btnLichSu.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn_a));
            btnLichSu.SetTextColor(Android.Graphics.Color.White);
            btnSanPham.SetTextColor(Android.Graphics.Color.Black);
            btnSanPham.Text = ("Sản Phẩm Yêu Thích");
            img_m.SetImageResource(Resource.Drawable.orange);
            img_g.SetImageResource(Resource.Drawable.apple_a);
            img_h.SetImageResource(Resource.Drawable.banana);
        }

        private void BtnKhuyenMai_Click(object sender, System.EventArgs e)
        {
            btnBanChay.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn));
            btnGiamGia.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn));
            btnKhuyenMai.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn_a));
            btnKhuyenMai.SetTextColor(Android.Graphics.Color.White);
            btnBanChay.Text = ("Bán Chạy");
            btnBanChay.SetTextColor(Android.Graphics.Color.Black);
            btnGiamGia.SetTextColor(Android.Graphics.Color.Black);
            btnGiamGia.Text = ("Giảm Giá");
            img_a.SetImageResource(Resource.Drawable.lemon);
            img_b.SetImageResource(Resource.Drawable.orange);
            img_c.SetImageResource(Resource.Drawable.banana);

        }

        private void BtnLike_Click(object sender, System.EventArgs e)
        {
            if (isLike == false)
            {
                btnLike.SetImageResource(Resource.Drawable.like_a);
                Toast.MakeText(this, "Đã thích", ToastLength.Short).Show();
                isLike = true;
            }
            else
            {
                btnLike.SetImageResource(Resource.Drawable.like);
                Toast.MakeText(this, "Bỏ thích", ToastLength.Short).Show();
                isLike = false;
            }
        }

        private void BtnGiamGia_Click(object sender, System.EventArgs e)
        {
            btnBanChay.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn));
            btnGiamGia.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn_a));
            btnKhuyenMai.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn));
            btnGiamGia.SetTextColor(Android.Graphics.Color.White);
            btnBanChay.Text = ("Bán Chạy");
            btnBanChay.SetTextColor(Android.Graphics.Color.Black);
            btnKhuyenMai.SetTextColor(Android.Graphics.Color.Black);
            btnKhuyenMai.Text = ("Khuyến Mại");
            img_a.SetImageResource(Resource.Drawable.mango);
            img_b.SetImageResource(Resource.Drawable.lemon);
            img_c.SetImageResource(Resource.Drawable.orange);
        }

        private void BtnBanChay_Click(object sender, System.EventArgs e)
        {
            btnBanChay.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn_a));
            btnBanChay.SetTextColor(Android.Graphics.Color.Black);
            btnGiamGia.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn));
            btnKhuyenMai.SetBackgroundDrawable(GetDrawable(Resource.Drawable.custom_btn));
            btnGiamGia.Text = ("Giảm Giá");
            btnGiamGia.SetTextColor(Android.Graphics.Color.Black);
            btnKhuyenMai.SetTextColor(Android.Graphics.Color.Black);
            btnKhuyenMai.Text = ("Khuyến Mại");
            img_a.SetImageResource(Resource.Drawable.apple_b);
            img_b.SetImageResource(Resource.Drawable.mango);
            img_c.SetImageResource(Resource.Drawable.orange);
        }
    }
}

